import copy
from operator import itemgetter, attrgetter
import string
import random
from validators.studentValidators import StudentValidators, StudentError
from validators.problemValidators import ProblemValidators, LabError


class ControllerError(Exception):
    '''
        clasa contine exceptia ce va fi ridicata in cazul unei erori
    '''
    pass


class StudentController:
    def __init__(self):
        self.__studentValidators = StudentValidators()

    def randomId(self, stList):
        letters = random.choice(string.ascii_letters) + random.choice(string.ascii_letters) + \
                  random.choice(string.ascii_letters) + random.choice(string.ascii_letters)
        numbers = random.randint(1000, 9999)

        nrMatricol = letters.lower() + str(numbers)

        try:
            self.__studentValidators.validateNewNrMatricol(nrMatricol, stList)
            return nrMatricol

        except StudentError:
            self.randomId(stList)

    def randomName(self):
        firstNames = ['Hassie', 'Domingo', 'Leena', 'Elina', 'Lucy', 'Tristan', 'Issac', 'Tressa', 'Lenora', 'Tamera']
        lastNames = ['Deanda', 'Shoults', 'Martensen', 'Lapointe', 'Melle', 'Bingman', 'Liriano', 'Hecker', 'Cifuentes',
                     'Sweetland']

        name = random.choice(firstNames) + ' ' + random.choice(lastNames)

        return name

    def randomGroup(self):
        grupa = str(random.randint(1, 3)) + str(random.randint(1, 3)) + str(random.randint(1, 7))

        return grupa


class ProblemController:
    def __init__(self):
        self.__problemValidators = ProblemValidators()

    def randomId(self, problemList):
        nrProblema = str(random.randint(1, 14)) + '_' + str(random.randint(1, 15))
        try:
            self.__problemValidators.validateNewNrProblema(nrProblema, problemList)
            return nrProblema
        except LabError:
            self.randomId(problemList)

    def randomDescription(self):
        lista = ['Problema 1', 'Problema 2', 'Problema 3', 'Problema 4', 'Problema 5', 'Problema 6', 'Problema 7'
            , 'Problema 8', 'Problema 9', 'Problema 10']

        return random.choice(lista)

    def randomDate(self):
        date = str(random.randint(2017, 2018)) + '-' + str(random.randint(1, 12)) + '-' + str(random.randint(1, 28))

        return date


class GradeController:
    '''
        sortByName(lista) - sorteaza lista pe baza numelui si returneaza lista sortata
        sortByGrade(lista) - sorteaza lista pe baza notelor si returneaza lista sortata
        medie(lista) - returneaza media notelor
    '''

    def __init__(self):
        self.__sortingAlgorithms = SortingAlgorithms()

    def randomGrade(self, listaStudenti, listaProbleme):
        listaSt = []
        listaPr = []

        for elem in listaStudenti:
            listaSt.append(elem)

        for elem in listaProbleme:
            listaPr.append(elem)

        nota = random.randint(1, 10)
        lista = []
        lista.append(random.choice(listaSt))
        lista.append(random.choice(listaPr))
        lista.append(str(nota))

        return lista

    def sortByName(self, lista):
        lista = self.__sortingAlgorithms.quickSortRec(lista, 0, len(lista) - 1, index=0)
        # lista = sorted(lista, key = itemgetter(0))
        return lista

    def sortByGrade(self, lista):
        # lista = self.__sortingAlgorithms.mergeHandler(lista, index = 1)
        lista = self.__sortingAlgorithms.bingoSort(lista, index=1)
        # lista = sorted(lista, key = itemgetter(1))
        return lista

    def medie(self, lista):
        medie = round(self.suma(lista) / len(lista), 2)
        return medie

    def suma(self, lista):
        if len(lista) == 0:
            return 0
        else:
            return lista[0] + self.suma(lista[1:])


class SortingAlgorithms:
    def __init__(self):
        pass

    def quickSortRec(self, l, left, right, index=0, reverse=False):
        if l == []:
            return []
        # partition the list
        pos = self.partition(l, left, right, index)
        # order the left part
        if left < pos - 1:
            self.quickSortRec(l, left, pos - 1, index)
        # order the right part
        if pos + 1 < right:
            self.quickSortRec(l, pos + 1, right, index)

        if reverse == True:
            l = l[::-1]
            print(l)

        return l

    def partition(self, l, left, right, index):
        """
        Split the values:
        smaller pivot greater
        return pivot position
        post: left we have < pivot
        right we have > pivot
        """
        pivot = l[left][index]
        i = left
        j = right
        while i != j:
            while l[j][index] >= pivot and i < j:
                j = j - 1
            l[i][index] = l[j][index]
            while l[i][index] <= pivot and i < j:
                i = i + 1
            l[j][index] = l[i][index]

        l[i][index] = pivot

        return i

    def mergeHandler(self, lista, index=0, reverse=False):
        finalList = self.mergeSort(lista, index)

        if reverse == True:
            finalList = finalList[::-1]

        return finalList

    def mergeSort(self, lista, index):
        middle = len(lista) // 2
        if len(lista) == 1:
            return lista

        lista1 = self.mergeSort(lista[:middle], index)
        lista2 = self.mergeSort(lista[middle:], index)

        return self.mergeAlgorithm(lista1, lista2, index)

    def mergeAlgorithm(self, l1, l2, index):
        sortedList = []
        i = 0
        j = 0

        while (i < len(l1) and j < len(l2)):
            if l1[i][index] < l2[j][index]:
                sortedList.append(l1[i])
                i += 1

            elif l1[i][index] > l2[j][index]:
                sortedList.append(l2[j])
                j += 1

            else:
                sortedList.append(l1[i])
                sortedList.append(l2[j])
                i += 1
                j += 1

        while i < len(l1):
            sortedList.append(l1[i])
            i += 1

        while j < len(l2):
            sortedList.append(l2[j])
            j += 1

        return sortedList

    def bingoSort(self, lista, index=0, reverse=False):
        max = len(lista) - 1
        nextValue = lista[max][index]
        for i in range(max - 1, -1, -1):
            if lista[i][index] > nextValue:
                nextValue = lista[i][index]

        while max > 0 and lista[max][index] == nextValue:
            max -= 1

        while max > 0:
            value = nextValue
            nextValue = lista[max][index]
            for i in range(max - 1, -1, -1):
                if lista[i][index] == value:
                    lista[i], lista[max] = lista[max], lista[i]
                    max -= 1
                elif lista[i][index] > nextValue:
                    nextValue = lista[i][index]
            while max > 0 and lista[max] == nextValue:
                max -= 1

        if reverse == True:
            lista = lista[::-1]

        return lista

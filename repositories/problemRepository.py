from domain.grades import GradeList
class ProblemRepo:
    def __init__(self, file, problemList):
        self.__file = file
        self.__problemList = problemList

    def __fileOpen(self):
        file = open(self.__file, 'r')
        try:
            for line in file:
                assignedProblem = line.split('/')
                self.__problemList.assignGrade(assignedProblem[0], assignedProblem[1])
        finally:
            file.close()


    def update(self, problemList):
        assignedProblems = problemList.getAllAssignedProblems()
        file = open(self.__file, 'w')
        try:
            for nrMatricol in assignedProblems:
                for nrProblema in assignedProblems[nrMatricol]:
                    file.write(nrMatricol + '/' + nrProblema + '\n')
        finally:
            file.close()
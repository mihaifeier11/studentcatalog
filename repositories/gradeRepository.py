from domain.grades import GradeList
import datetime
class GradeRepositoryError(Exception):
    pass

class GradeRepo:
    '''
        repository pentru note
        __fileOpen() - adauga note;e din fisier in memorie
        update() - rescrie fisierul cu noile date
    '''
    def __init__(self, file, gradeList):
        self.__file = file
        self.__gradeList = gradeList
        self.__fileOpen()

    def __fileOpen(self):
        fileGrades = open(self.__file, 'r')
        try:
            for line in fileGrades:
                line = line.split('/')
                self.__gradeList.addGrade(line[0], line[1], int(line[2]))

        finally:
            fileGrades.close()


    def update(self, gradeList):
        fileGrades = open(self.__file, 'w')

        listOfGrades = gradeList.getAll()
        try:
            for student in listOfGrades:
                for nrLab in listOfGrades[student]:
                    fileGrades.write(student + '/' + nrLab + '/' + str(listOfGrades[student][nrLab]) + '\n')
        finally:
            fileGrades.close()

from domain.lab import Probleme, ProblemaLab
import datetime
class LabRepositoryError(Exception):
    pass

class LabRepo:
    '''
        repository pentru probleme
        __fileOpen() - adauga studentii din fisier in memorie
        update() - rescrie fisierul cu noile date
        find(numarulProblemei) - returneaza enuntul problemei si data pana cand aceasta trebuie predata
        delete(numarulProblemei) - sterge problema care are id-ul dat
    '''
    def __init__(self, file, problemList):
        self.__file = file
        self.__problemList = problemList
        self.__fileOpen()

    def __fileOpen(self):
        fileProblem = open(self.__file, 'r')
        try:
            for line in fileProblem:
                line = line.split('/')
                line[2] = line[2].split('-')
                self.__problemList.addProblema(line[0], line[1],
                                               datetime.date(year = int(line[2][0]), month = int(line[2][1]), day = int(line[2][2])))

        finally:
            fileProblem.close()


    def update(self, problemList):
        fileProblem = open(self.__file, 'w')

        listOfProblems = problemList.getAll()
        try:
            for elem in listOfProblems:
                fileProblem.write(elem + '/' + listOfProblems[elem][0] + '/' + str(listOfProblems[elem][1]) + '\n')
        finally:
            fileProblem.close()


    def find(self, nrProblema):
        fileProblem = open(self.__file, 'r')
        try:
            for line in fileProblem:
                line = line.split('/')
                if line[0] == nrProblema:
                    return line[1] + '/' + str(line[2])
        finally:
            fileProblem.close()

        raise LabRepositoryError('Numarul introdus nu exista.')


    def delete(self, nrProblema):
        try:
            fileProblem = open(self.__file, 'r')
            problemList = fileProblem.readlines()
        finally:
            fileProblem.close()

        fileProblem = open(self.__file, 'w')
        try:
            for line in problemList:
                tempLine = line.split('/')
                if tempLine[0] != nrProblema:
                    fileProblem.write(line)
        finally:
            fileProblem.close()


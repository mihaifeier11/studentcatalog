from domain.student import Student, Catalog
class StudentRepositoryError(Exception):
    pass

class StudentRepo:
    '''
        repository pentru studenti
        __fileOpen(clasaCatalog) - adauga studentii din fisier in memorie
        update() - rescrie fisierul cu noile date
        find(numarulMatricol) - returneaza numele si grupa studentului cu numarul matricol respectiv, raise Error daca nu exista
        delete(numarulMatricol) - sterge studentul ce are numarul matricol dat din fisier
    '''
    def __init__(self, file, studentList):
        self.__file = file
        self.__studentList = studentList
        self.__fileOpen()

    def __fileOpen(self):
        fileStudent = open(self.__file, 'r')
        try:
            for line in fileStudent:
                line = line.split('/')
                self.__studentList.addStudent(line[0], line[1], int(line[2]))

        finally:
            fileStudent.close()


    def update(self, studentList):
        fileStudent = open(self.__file, 'w')

        listOfStudents = studentList.getAll()
        try:
            for elem in listOfStudents:
                fileStudent.write(elem + '/' + listOfStudents[elem][0] + '/' + str(listOfStudents[elem][1]) + '\n')
        finally:
            fileStudent.close()


    def find(self, nrMatricol):
        fileStudent = open(self.__file, 'r')
        try:
            for line in fileStudent:
                line = line.split(',')
                if line[0] == nrMatricol:
                    return line[1] + ', ' + str(line[2])
        finally:
            fileStudent.close()

        raise StudentRepositoryError('Numarul matricol introdus nu exista.')


    def delete(self, nrMatricol):
        try:
            fileStudent = open(self.__file, 'r')
            studentList = fileStudent.readlines()
        finally:
            fileStudent.close()

        try:
            fileStudent = open(self.__file, 'w')
            for line in studentList:
                tempLine = line.split('/')
                if tempLine[0] != nrMatricol:
                    fileStudent.write(line)
        finally:
            fileStudent.close()


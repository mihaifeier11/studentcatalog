class Grades:
    '''
        clasa retine numarul laboratorului si nota primita
        getGrade(nrLab) - returneaza nota la laboratorul dat
            nrLab - numarul laboratorului

        getAll() - returneaza numarul laboratoarelor si notele la acestea

        addGrade(nrLab, grade) - adauga o nota la un laborator
            nrLab - numarul laboratorului
            grade - nota primita

        modifyGrade(nrLab, grade) - modifica nota la un laborator
            nrLab - numarul laboratorului
            grade - nota primita

        removeGrade(nrLab) - sterge problema asignata si nota primita, daca exista
            nrLab - numarul laboratorului
    '''
    def __init__(self):
        self.__grades = {}
        self.__problems = []


    def getGrade(self, nrLab):
        return self.__grades[nrLab]


    def getAll(self):
        return self.__grades


    def addGrade(self, nrLab, grade):
        self.__grades[nrLab] = grade


    def assignProblem(self, nrLab):
        self.__problems.append(nrLab)


    def getAssignedProblems(self):
        return self.__problems

    def removeAssignedProblem(self, nrLab):
        self.__problems.remove(nrLab)


    def modifyGrade(self, nrLab, grade):
        self.__grades[nrLab] = grade


    def removeProblem(self, nrLab):
        del self.__grades[nrLab]




class GradeList:
    '''
        clasa retine numarul matricol al studentului si adauga note la laborator

        getGrade(numarMatricol, numarLaborator) - returneaza nota primita la laboratorul dat
            numarMatricol - numarul matricol al studentului
            numarLaborator - numarul laboratorului

        getGrades(numarMatricol) - returneaza problemele asignate si notele la fiecare problema
            numarMatricol - numarul matricol al studentului

        addGrade(numarMatricol, numarLaborator, nota) - adauga o nota studentului cu numarul matricol dat
            numarMatricol - numarul matricol al studentului
            numarLaborator - numarul laboratorului
            nota - nota la laboratorul dat

        assignProblem(numarMatricol, numarLaborator) - asigneaza studentului o problema
            numarMatricol - numarul matricol al studentului
            numarLaborator - numarul laboratorului

        modifyGrade(numarMatricol, numarLaborator, nota) - modifica nota studentului la o problema
            numarMatricol - numarul matricol al studentului
            numarLaborator - numarul laboratorului
            nota - noua nota

        deleteProblem(numarMatricol, numarLaborator) - sterge problema asignata si nota, daca exista
            numarMatricol - numarul matricol al studentului
            numarLaborator - numarul laboratorului
    '''
    def __init__(self):
        self.__gradeList = {}


    def addGrade(self, nrMatricol, nrProblema, nota):
        if nrMatricol in self.__gradeList:
            self.__gradeList[nrMatricol].addGrade(nrProblema, nota)
        else:
            self.__gradeList[nrMatricol] = Grades()
            self.__gradeList[nrMatricol].addGrade(nrProblema, nota)


    def assignGrade(self, nrMatricol, nrProblema):
        if nrMatricol in self.__gradeList:
            self.__gradeList[nrMatricol].assignProblem(nrProblema)
        else:
            self.__gradeList[nrMatricol] = Grades()
            self.__gradeList[nrMatricol].assignProblem(nrProblema)


    def removeAssignedProblem(self, nrMatricol, nrProblema):
        self.__gradeList[nrMatricol].removeAssignedProblem(nrProblema)

    def getGrade(self, nrMatricol, nrProblema):
        return self.__gradeList[nrMatricol].getGrade(nrProblema)


    def getGrades(self, nrMatricol):
        return self.__gradeList[nrMatricol].getAll()


    def getAssignedProblems(self, nrMatricol):
        return self.__gradeList[nrMatricol].getAssignedProblems()


    def getAllAssignedProblems(self):
        assignedProblems = {}
        for elem in self.__gradeList:
            assignedProblems[elem] = self.__gradeList[elem].getAssignedProblems()

        return assignedProblems

    def hasProblemAssigned(self, nrMatricol, nrProblema):
        try:
            self.__gradeList[nrMatricol].getGrade(nrProblema)
            return True
        except KeyError:
            return False

    def hasGrades(self, nrMatricol):
        try:
            self.__gradeList[nrMatricol].getAll()
            return True
        except KeyError:
            return False

    def getAll(self):
        gradesList = {}
        for elem in self.__gradeList:
            gradesList[elem] = self.__gradeList[elem].getAll()

        return gradesList

    def modifyGrade(self, nrMatricol, nrProblema, nota):
        self.__gradeList[nrMatricol].modifyGrade(nrProblema, nota)


    def modifyNrProblema(self, nrMatricol, nrProblema, newNrProblema):
        self.__gradeList[nrMatricol].modifyNrProblema(nrProblema, newNrProblema)


    def modifyNrMatricol(self, nrMatricol, newNrMatricol):
        self.__gradeList[newNrMatricol] = self.__gradeList[nrMatricol]
        del self.__gradeList[nrMatricol]

    def removeProblem(self, nrMatricol, nrProblema):
        self.__gradeList[nrMatricol].removeProblem(nrProblema)

    def removeGrades(self, nrMatricol):
        del self.__gradeList[nrMatricol]

    def removeGrade(self, nrMatricol, nrProblema):
        self.__gradeList[nrMatricol].removeGrade(nrProblema)
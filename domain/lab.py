class ProblemaLab:
    '''
        clasa creeaza o problema de laborator cu urmatorii parametrii:
            descriere - enuntul problemei
            deadline - data pana cand trebuie predata problema

        getDescriere() - returneaza enuntul problemei

        getDeadline() - returneaza data pana cand trebuie predata problema

        modifyDescriere(newDescriere) - modifica enuntul problemei
            newDescriere - noul enunt al problemei

        modifyDeadline(newDeadline) - modifica data pana cand trebuie predata problema
            newDeadline - noua data

    '''
    def __init__(self, descriere, deadline):
        self.__descriere = descriere
        self.__deadline = deadline


    def getNrLab(self):
        return self.__nrLab


    def getDescriere(self):
        return self.__descriere


    def getDeadline(self):
        return self.__deadline


    def modifyNrLab(self, newNrLab):
        self.__nrLab = newNrLab


    def modifyDescriere(self, newDesc):
        self.__descriere = newDesc


    def modifyDeadline(self, newDeadline):
        self.__deadline = newDeadline



class Probleme:
    '''
        addProblema(nrLab, descriere, deadline) - adauga o problema
            nrLab - numarul laboratorului
            descriere - enuntul problemei
            deadline - data pana cand trebuie predata problema

        getProblemaDescriere(nrLab) - returneaza enuntul problemei
            nrLab - numarul laboratorului

        getProblemaDeadline(nrLab) - returneaza data pana cand trebuie predata problema
            nrLab - numarul laboratorului


        modifyProblemaNr(nrLab, newNrLab) - modifica numarul problemei
            nrLab - numarul laboratorului
            newNrLab - noul numar al problemei

        modifyProblemaDescriere(nrLab, newDesc) - modifica descrierea problemei
            nrLab - numarul laboratorului
            newDesc - noul enunt al problemei

        modifyProblemaDeadline(nrLab, newDeadline) - modifica data pana cand problema trebuie predata
            nrLab - numarul laboratorului
            newDeadline - noua data

        deleteProblema(nrLab) - sterge problema din lista
            nrLab - numarul laboratorului

        getProblema(nrLab) - returneaza id-ul problemei, enuntul si data pana cand problema trebuie predata
            nrLab - numarul laboratorului

        getAll() - returneaza o lista ce contine toata problemele
    '''
    def __init__(self):
        self.__probleme = {}


    def addProblema(self, nrLab, descriere, deadline):
        self.__probleme[nrLab] = ProblemaLab(descriere, deadline)


    def getProblemaDescriere(self, nrLab):
        return self.__probleme[nrLab].getDescriere()


    def getProblemaDeadline(self, nrLab):
        return self.__probleme[nrLab].getDeadline()


    def modifyProblemaNr(self, nrLab, newNrLab):
        self.__probleme[newNrLab] = self.__probleme[nrLab]
        del self.__probleme[nrLab]


    def modifyProblemaDescriere(self, nrLab, newDesc):
        self.__probleme[nrLab].modifyDescriere(newDesc)


    def modifyProblemaDeadline(self, nrLab, newDeadline):
        self.__probleme[nrLab].modifyDeadline(newDeadline)


    def deleteProblema(self, nrLab):
        del self.__probleme[nrLab]


    def getProblema(self, nrLab):
        problema = []
        problema.append(self.getProblemaDescriere(nrLab))
        problema.append(str(self.getProblemaDeadline(nrLab)))
        return problema

    def getAll(self):
        problemList = {}
        for elem in self.__probleme:
            problemList[elem] = self.getProblema(elem)

        return problemList

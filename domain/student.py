import copy

class Catalog:
    '''
        creeaza un catalog cu studenti, indexul fiind numarul matricol, parametri:
            numarulMatricol - numarul matricol al studentului
            nume - numele studentului
            grupa - grupa studentului

        addStudent(numarMatricol, nume, grupa) - creeaza un student
        modifyStudentNrMatricol(numarulMatricolActual, noulNumeMatricol) - modifica numarul matricol al studentului
        modifyStudentNume(numarulMatricol, noulNume) - modifica numele studentului
        modifyStudentGrupa(numarulMatricol, nouaGrupa) - modifica grupa studentului
        deleteStudent(numarulMatricol) - sterge studentul
        getStudent(numarulMatricol) - returneaza o lista ce contine numarul matricol, numele si grupa
        getAll() - returneaza o lista de liste continand fiecare student
    '''
    def __init__(self):
        self.__backupList = []
        self.__catalog = {}


    def addStudent(self, nrMatricol, nume, grupa):
        self.__catalog[nrMatricol] = Student(nume, grupa)

    def getStudentNume(self, nrMatricol):
        return self.__catalog[nrMatricol].getNume()


    def getStudentGrupa(self, nrMatricol):
        return self.__catalog[nrMatricol].getGrupa()


    def modifyStudentNrMatricol(self, nrMatricol, newNrMatricol):
        self.__catalog[newNrMatricol] = self.__catalog[nrMatricol]
        del self.__catalog[nrMatricol]


    def modifyStudentNume(self, nrMatricol, newName):
        self.__catalog[nrMatricol].modifyName(newName)


    def modifyStudentGrupa(self, nrMatricol, newGrupa):
        self.__catalog[nrMatricol].modifyGrupa(newGrupa)


    def deleteStudent(self, nrMatricol):
        del self.__catalog[nrMatricol]


    def getStudent(self, nrMatricol):
        student = []

        student.append(self.__catalog[nrMatricol].getNume())
        student.append(self.__catalog[nrMatricol].getGrupa())

        return student


    def getAll(self):
        students = {}

        for elem in self.__catalog:
            students[elem] = self.getStudent(elem)

        return students


class Student:
    '''
        creeaza un student, clasa are 2 parametri:
            nume - numele studentului
            grupa - grupa studentului

        getNume() - returneaza numele studentului
        getGrupa() - returneaza grupa studentului
        modifyName(noulNume) - modifica numele studentului
        modifyGrupa(nouaGrupa) - modifica grupa studentului

    '''
    def __init__(self, nume, grupa):
        self.__nume = nume
        self.__grupa = grupa


    def getNume(self):
        return self.__nume


    def getGrupa(self):
        return self.__grupa


    def modifyName(self, newNume):
        self.__nume = newNume


    def modifyGrupa(self, newGrupa):
        self.__grupa = newGrupa


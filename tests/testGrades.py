import unittest
from domain.grades import Grades, GradeList


class testGrades(unittest.TestCase):
    def setUp(self):
        self.__grades = Grades()
        self.__grades.addGrade('1_1', 9)
        self.__grades.addGrade('2_1', 10)
        self.__grades.addGrade('3_1', 10)

        self.__gradeList = GradeList()

        self.__gradeList.addGrade('abcd1234', '3_3', 10)
        self.__gradeList.addGrade('abcd1234', '3_6', 9)
        self.__gradeList.addGrade('abcd1234', '3_4', 8)


    def testGetGrade(self):
        self.assertEqual(self.__grades.getGrade('1_1'), 9)
        self.assertEqual(self.__grades.getGrade('2_1'), 10)
        self.assertEqual(self.__grades.getGrade('3_1'), 10)


    def testAddGrade(self):
        self.__grades.addGrade('1_2', 10)
        self.__grades.addGrade('2_2', 10)
        self.__grades.addGrade('3_2', 10)
        self.assertEqual(self.__grades.getGrade('1_2'), 10)
        self.assertEqual(self.__grades.getGrade('2_2'), 10)
        self.assertEqual(self.__grades.getGrade('3_2'), 10)


    def testModifyGrade(self):
        self.__grades.modifyGrade('1_1', 2)
        self.__grades.modifyGrade('2_1', 3)
        self.__grades.modifyGrade('3_1', 4)

        self.assertEqual(self.__grades.getGrade('1_1'), 2)
        self.assertEqual(self.__grades.getGrade('2_1'), 3)
        self.assertEqual(self.__grades.getGrade('3_1'), 4)


    def testModifyNrProblema(self):
        self.__grades.modifyNrProblema('1_1', '1_2')
        self.__grades.modifyNrProblema('2_1', '2_2')
        self.__grades.modifyNrProblema('3_1', '3_2')
        self.assertEqual(self.__grades.getGrade('1_2'), 9)
        self.assertEqual(self.__grades.getGrade('2_2'), 10)
        self.assertEqual(self.__grades.getGrade('3_2'), 10)


    def testRemoveGrade(self):
        self.__grades.removeGrade('1_1')
        self.assertRaises(KeyError, lambda: self.__grades.getGrade('1_1'))

        self.__grades.removeGrade('2_1')
        self.assertRaises(KeyError, lambda: self.__grades.getGrade('2_1'))

        self.__grades.removeGrade('3_1')
        self.assertRaises(KeyError, lambda: self.__grades.getGrade('3_1'))


    def testGetGradeFromGradeList(self):
        self.assertEqual(self.__gradeList.getGrade('abcd1234', '3_3'), 10)
        self.assertEqual(self.__gradeList.getGrade('abcd1234', '3_6'), 9)
        self.assertEqual(self.__gradeList.getGrade('abcd1234', '3_4'), 8)


    def testGetGradesFromGradeList(self):
        self.assertEqual(self.__gradeList.getGrades('abcd1234'), {'3_3': 10, '3_6': 9, '3_4': 8})


    def testAddGradeFromGradeList(self):
        self.__gradeList.addGrade('aaaa1111', '1_1', 9)
        self.__gradeList.addGrade('aaaa1111', '2_1', 10)
        self.__gradeList.addGrade('aaaa1111', '3_1', 8)
        self.assertEqual(self.__gradeList.getGrade('aaaa1111', '1_1'), 9)
        self.assertEqual(self.__gradeList.getGrade('aaaa1111', '2_1'), 10)
        self.assertEqual(self.__gradeList.getGrade('aaaa1111', '3_1'), 8)


    def testModifyGradeFromGradeList(self):
        self.__gradeList.modifyGrade('abcd1234', '3_3', 3)
        self.__gradeList.modifyGrade('abcd1234', '3_6', 4)
        self.__gradeList.modifyGrade('abcd1234', '3_4', 5)

        self.assertEqual(self.__gradeList.getGrade('abcd1234', '3_3'), 3)
        self.assertEqual(self.__gradeList.getGrade('abcd1234', '3_6'), 4)
        self.assertEqual(self.__gradeList.getGrade('abcd1234', '3_4'), 5)


    def testModifyNrProblemaFromGradeList(self):
        self.__gradeList.modifyNrProblema('abcd1234', '3_3', '4_3')
        self.__gradeList.modifyNrProblema('abcd1234', '3_6', '4_6')
        self.__gradeList.modifyNrProblema('abcd1234', '3_4', '4_4')

        self.assertEqual(self.__gradeList.getGrade('abcd1234', '4_3'), 10)
        self.assertEqual(self.__gradeList.getGrade('abcd1234', '4_6'), 9)
        self.assertEqual(self.__gradeList.getGrade('abcd1234', '4_4'), 8)


    def testModifyNrMatricolFromGradeList(self):
        self.__gradeList.modifyNrMatricol('abcd1234', 'aaaa1111')
        self.assertEqual(self.__gradeList.getGrade('aaaa1111', '3_3'), 10)
        self.assertEqual(self.__gradeList.getGrade('aaaa1111', '3_6'), 9)
        self.assertEqual(self.__gradeList.getGrade('aaaa1111', '3_4'), 8)
        self.assertRaises(KeyError, lambda: self.__gradeList.getGrade('abcd1234', '3_3'))
        self.assertRaises(KeyError, lambda: self.__gradeList.getGrade('abcd1234', '3_6'))
        self.assertRaises(KeyError, lambda: self.__gradeList.getGrade('abcd1234', '3_4'))


    def testRemoveGradeFromGradeList(self):
        self.__gradeList.removeGrade('abcd1234', '3_3')
        self.__gradeList.removeGrade('abcd1234', '3_6')
        self.__gradeList.removeGrade('abcd1234', '3_4')

        self.assertRaises(KeyError, lambda: self.__gradeList.getGrade('abcd1234', '3_3'))
        self.assertRaises(KeyError, lambda: self.__gradeList.getGrade('abcd1234', '3_6'))
        self.assertRaises(KeyError, lambda: self.__gradeList.getGrade('abcd1234', '3_4'))


    def testRemoveGradesFromGradeList(self):
        self.__gradeList.removeGrades('abcd1234')
        self.assertRaises(KeyError, lambda: self.__gradeList.getGrades('abcd1234'))

if __name__ == '__main__':
    unittest.main()

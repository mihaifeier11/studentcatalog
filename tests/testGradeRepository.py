import unittest
from repositories.gradeRepository import GradeRepo
from domain.grades import GradeList
class MyTestCase(unittest.TestCase):
    def setUp(self):
        file = open('testGrades.txt', 'w')
        file.write('asdf1234/3_2/9/\n')
        file.write('vbnm1345/3_2/10/\n')
        file.write('aaaa1111/4_2/10/\n')
        file.close()
        self.__GradeList = GradeList()
        self.__gradeRepo = GradeRepo('testGrades.txt', self.__GradeList)

    def testFileOpen(self):
        self.assertEqual(self.__GradeList.getGrades('asdf1234'),{'3_2': '9'})
        self.assertEqual(self.__GradeList.getGrades('vbnm1345'), {'3_2': '10'})
        self.assertEqual(self.__GradeList.getGrades('aaaa1111'), {'4_2': '10'})


    def testUpdate(self):
        self.__GradeList.modifyNrMatricol('asdf1234', 'aaaa2222')
        self.__gradeRepo.update(self.__GradeList)



if __name__ == '__main__':
    unittest.main()

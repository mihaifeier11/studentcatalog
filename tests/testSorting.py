import unittest
from controllers.controller import SortingAlgorithms
import random
from operator import itemgetter, attrgetter

class SortingTest(unittest.TestCase):
    def setUp(self):
        self.__sorting = SortingAlgorithms()

    def testQuicksort(self):
        lista = []
        for i in range(3):
            for j in range (random.randint(10, 20)):
                l = []
                l.append(random.randint(1, 1000))
                l.append(random.randint(1, 1000))
                lista.append(l)
            l = self.__sorting.quickSortRec(lista, 0, len(lista) - 1)
            self.assertEqual(l, sorted(l, key = itemgetter(0)))

        for i in range(3):
            for j in range (random.randint(10, 20)):
                l = []
                l.append(random.randint(1, 1000))
                l.append(random.randint(1, 1000))
                lista.append(l)
            l = self.__sorting.quickSortRec(lista, 0, len(lista) - 1, index = 1)
            self.assertEqual(l, sorted(l, key = itemgetter(1)))

    def testMergeSort(self):
        lista = []
        for i in range(3):
            for j in range (random.randint(10, 20)):
                l = []
                l.append(random.randint(1, 1000))
                l.append(random.randint(1, 1000))
                lista.append(l)
            l = self.__sorting.mergeHandler(lista)
            self.assertEqual(l, sorted(l, key = itemgetter(0)))

        for i in range(3):
            for j in range (random.randint(10, 20)):
                l = []
                l.append(random.randint(1, 1000))
                l.append(random.randint(1, 1000))
                lista.append(l)
            l = self.__sorting.mergeHandler(lista, index = 1)
            self.assertEqual(l, sorted(l, key = itemgetter(1)))


if __name__ == '__main__':
    unittest.main()

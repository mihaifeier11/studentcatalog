import unittest
from domain.lab import Probleme
from repositories.labRepository import LabRepo, LabRepositoryError
import datetime

class MyTestCase(unittest.TestCase):
    def setUp(self):
        file = open('testProblems.txt', 'w')
        file.write('1_1/Descriere problema 1/2017-11-14\n')
        file.write('2_1/Descriere problema 2/2017-11-21\n')
        file.write('3_1/Descriere problema 3/2017-11-28\n')
        file.close()
        self.__probleme = Probleme()
        self.__problemRepository = LabRepo('testProblems.txt', self.__probleme)


    def testFileOpen(self):
        self.assertEqual(self.__probleme.getAll(), {'1_1': ['Descriere problema 1', '2017-11-14'],
                                                    '2_1': ['Descriere problema 2', '2017-11-21'],
                                                    '3_1': ['Descriere problema 3', '2017-11-28']})


    def testUpdate(self):
        self.__probleme.modifyProblemaNr('1_1', '1_2')
        self.__probleme.modifyProblemaNr('2_1', '2_2')
        self.__probleme.modifyProblemaNr('3_1', '3_2')
        self.__problemRepository.update(self.__probleme)

        file = open('testProblems.txt', 'r')
        lines = file.readlines()
        self.assertEqual(lines[0], '1_2/Descriere problema 1/2017-11-14\n')
        self.assertEqual(lines[1], '2_2/Descriere problema 2/2017-11-21\n')
        self.assertEqual(lines[2], '3_2/Descriere problema 3/2017-11-28\n')


    def testFind(self):
        self.assertEqual(self.__problemRepository.find('1_1'), 'Descriere problema 1/2017-11-14\n')
        self.assertEqual(self.__problemRepository.find('2_1'), 'Descriere problema 2/2017-11-21\n')
        self.assertEqual(self.__problemRepository.find('3_1'), 'Descriere problema 3/2017-11-28\n')


    def testDelete(self):
        self.__problemRepository.delete('1_1')
        file = open('testProblems.txt', 'r')
        lines = file.readlines()
        self.assertEqual(lines[0], '2_1/Descriere problema 2/2017-11-21\n')
        self.assertEqual(lines[1], '3_1/Descriere problema 3/2017-11-28\n')

if __name__ == '__main__':
    unittest.main()

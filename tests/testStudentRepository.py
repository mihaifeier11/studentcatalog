import unittest
from domain.student import Catalog
from repositories.studentRepository import StudentRepo, StudentRepositoryError

class TestStudentRepository(unittest.TestCase):
    def setUp(self):
        file = open('testStudents.txt', 'w')
        file.write('abcd1234/Waylon Dalton/211\n')
        file.write('aaaa1111/Marcus Cruz/211\n')
        file.write('bbbb2222/Eddie Randolph/211\n')
        file.close()
        self.__catalog = Catalog()
        self.__studentRepository = StudentRepo('testStudents.txt', self.__catalog)


    def testFileOpen(self):
        self.assertEqual(self.__catalog.getAll(), {'abcd1234': ['Waylon Dalton', 211],
                                                   'aaaa1111': ['Marcus Cruz', 211],
                                                   'bbbb2222': ['Eddie Randolph', 211]})


    def testUpdate(self):
        self.__catalog.modifyStudentNume('abcd1234', 'Abdullah Lang')
        self.__catalog.modifyStudentNume('bbbb2222', 'Mike Johnson')
        self.__studentRepository.update(self.__catalog)
        file = open('testStudents.txt', 'r')
        student = file.read()
        self.assertEqual(student, 'abcd1234/Abdullah Lang/211\naaaa1111/Marcus Cruz/211\nbbbb2222/Mike Johnson/211\n')

    #
    # def testFind(self):
    #     self.assertEqual(self.__studentRepository.find('abcd1234'), 'Waylon Dalton, 211\n')
    #     self.assertEqual(self.__studentRepository.find('aaaa1111'), 'Marcus Cruz, 211\n')
    #     self.assertEqual(self.__studentRepository.find('bbbb2222'), 'Eddie Randolph, 211\n')
    #
    #     self.assertRaises(StudentRepositoryError, lambda: self.__studentRepository.find('test'))


    def testDelete(self):
        self.__studentRepository.delete('abcd1234')
        line = []
        try:
            fileStudent = open('testStudents.txt', 'r')
            line.append(fileStudent.readline())
            line.append(fileStudent.readline())
        finally:
            fileStudent.close()

        self.assertEqual(line[0], 'aaaa1111/Marcus Cruz/211\n')
        self.assertEqual(line[1], 'bbbb2222/Eddie Randolph/211\n')
if __name__ == '__main__':
    unittest.main()


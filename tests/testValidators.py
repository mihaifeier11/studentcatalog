import unittest
from validators.studentValidators import StudentValidators, StudentError
from validators.problemValidators import ProblemValidators, LabError
class testStudentValidators(unittest.TestCase):
    def setUp(self):
        self.__studentValidators = StudentValidators()


    def testValidateNrMatricol(self):
        stList = {'fmir1234': 'Nume Prenume Grupa',
                  'asdf1234': 'Nume Prenume Grupa',
                  'aaaa1111': 'Nume Prenume Grupa',
                  'bbbb2222': 'Nume Prenume Grupa'}
        self.assertRaises(StudentError, lambda: self.__studentValidators.validateNewNrMatricol('asd', stList))
        self.assertRaises(StudentError, lambda: self.__studentValidators.validateNewNrMatricol('123d', stList))
        self.assertRaises(StudentError, lambda: self.__studentValidators.validateNewNrMatricol('   ', stList))


    def testNewValidateNrMatricol(self):
        stList = {'fmir1234': 'Nume Prenume Grupa',
                  'asdf1234': 'Nume Prenume Grupa',
                  'aaaa1111': 'Nume Prenume Grupa',
                  'bbbb2222': 'Nume Prenume Grupa'}
        # self.assertRaises(StudentError, lambda: self.__studentValidators.validateNrMatricol('fmir1234', stList))
        # self.assertRaises(StudentError, lambda: self.__studentValidators.validateNrMatricol('asdf1234', stList))
        # self.assertRaises(StudentError, lambda: self.__studentValidators.validateNrMatricol('aaaa1111', stList))



    def testValidateNume(self):
        self.assertRaises(StudentError, lambda: self.__studentValidators.validateNume('Nume. Prenume'))
        self.assertRaises(StudentError, lambda: self.__studentValidators.validateNume('Num3 Prenume'))
        self.assertRaises(StudentError, lambda: self.__studentValidators.validateNume('Nume  Prenume'))


    def testValidateGrupa(self):
        self.assertRaises(StudentError, lambda: self.__studentValidators.validateGrupa('213a'))
        self.assertRaises(StudentError, lambda: self.__studentValidators.validateGrupa('213.'))
        self.assertRaises(StudentError, lambda: self.__studentValidators.validateGrupa('21 3'))


class TestLabValidators(unittest.TestCase):
    def setUp(self):
        self.__labValidators = ProblemValidators()

    def testValidareNrProblema(self):
        problemList = {'1_2': 'Descriere Deadline',
                       '1_1': 'Descriere Deadline',
                       '1_4': 'Descriere Deadline',
                       '1_3': 'Descriere Deadline',}

        self.assertRaises(LabError, lambda: self.__labValidators.validateNrProblema('3+2', problemList))
        self.assertRaises(LabError, lambda: self.__labValidators.validateNrProblema('3_2_2', problemList))
        self.assertRaises(LabError, lambda: self.__labValidators.validateNrProblema('asd', problemList))
        self.assertRaises(LabError, lambda: self.__labValidators.validateNrProblema('1_6', problemList))


    def testValidareNewNrProblema(self):
        problemList = {'1_2': 'Descriere Deadline',
                       '1_1': 'Descriere Deadline',
                       '1_4': 'Descriere Deadline',
                       '1_3': 'Descriere Deadline',}
        self.assertRaises(LabError, lambda: self.__labValidators.validateNewNrProblema('3+2', problemList))
        self.assertRaises(LabError, lambda: self.__labValidators.validateNewNrProblema('3_2_2', problemList))
        self.assertRaises(LabError, lambda: self.__labValidators.validateNewNrProblema('asd', problemList))
        self.assertRaises(LabError, lambda: self.__labValidators.validateNewNrProblema('1_2', problemList))


    def testValidateDeadline(self):
        self.assertRaises(LabError, lambda: self.__labValidators.validateDeadline('22-22-2017'))
        self.assertRaises(LabError, lambda: self.__labValidators.validateDeadline('29-02-2017'))
        self.assertRaises(LabError, lambda: self.__labValidators.validateDeadline('33-1-2017'))

if __name__ == '__main__':
    unittest.main()

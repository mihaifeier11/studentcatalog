import unittest

from domain.student import Student, Catalog


class TestCatalog(unittest.TestCase):
    def setUp(self):
        self.__catalog = Catalog()
        self.__catalog.addStudent('dddd1111', 'Mathias Little', 222)
        self.__catalog.addStudent('eeee1111', 'Lia Shelton', 232)
        self.__catalog.addStudent('ffff1111', 'Jonathon Sheppard', 212)


    def testGetStudentNume(self):
        self.assertEqual(self.__catalog.getStudentNume('dddd1111'), 'Mathias Little')
        self.assertEqual(self.__catalog.getStudentNume('eeee1111'), 'Lia Shelton')
        self.assertEqual(self.__catalog.getStudentNume('ffff1111'), 'Jonathon Sheppard')


    def testGetStudentGrupa(self):
        self.assertEqual(self.__catalog.getStudentGrupa('dddd1111'), 222)
        self.assertEqual(self.__catalog.getStudentGrupa('eeee1111'), 232)
        self.assertEqual(self.__catalog.getStudentGrupa('ffff1111'), 212)


    def testAddStudent(self):
        self.__catalog.addStudent('aaaa1111', 'Angela Walker', 222)
        self.__catalog.addStudent('bbbb2222', 'Joanna Shaffer', 223)
        self.__catalog.addStudent('cccc3333', 'Abdullah Lang', 224)

        self.assertEqual(self.__catalog.getStudentNume('aaaa1111'), 'Angela Walker')
        self.assertEqual(self.__catalog.getStudentNume('bbbb2222'), 'Joanna Shaffer')
        self.assertEqual(self.__catalog.getStudentNume('cccc3333'), 'Abdullah Lang')

        self.assertEqual(self.__catalog.getStudentGrupa('aaaa1111'), 222)
        self.assertEqual(self.__catalog.getStudentGrupa('bbbb2222'), 223)
        self.assertEqual(self.__catalog.getStudentGrupa('cccc3333'), 224)


    def testModifyStudentNrMatricol(self):
        self.__catalog.modifyStudentNrMatricol('dddd1111', 'dddd2222')
        self.assertEqual(self.__catalog.getStudentNume('dddd2222'), 'Mathias Little')
        self.assertEqual(self.__catalog.getStudentGrupa('dddd2222'), 222)
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentNume('dddd1111'))
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentGrupa('dddd1111'))

        self.__catalog.modifyStudentNrMatricol('eeee1111', 'eeee2222')
        self.assertEqual(self.__catalog.getStudentNume('eeee2222'), 'Lia Shelton')
        self.assertEqual(self.__catalog.getStudentGrupa('eeee2222'), 232)
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentNume('eeee1111'))
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentGrupa('eeee1111'))

        self.__catalog.modifyStudentNrMatricol('ffff1111', 'ffff2222')
        self.assertEqual(self.__catalog.getStudentNume('ffff2222'), 'Jonathon Sheppard')
        self.assertEqual(self.__catalog.getStudentGrupa('ffff2222'), 212)
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentNume('ffff1111'))
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentGrupa('ffff1111'))


    def testModifyStudentNume(self):
        self.__catalog.modifyStudentNume('dddd1111', 'Alonzo Dudley')
        self.assertEqual(self.__catalog.getStudentNume('dddd1111'), 'Alonzo Dudley')

        self.__catalog.modifyStudentNume('eeee1111', 'Carolyn Shaw')
        self.assertEqual(self.__catalog.getStudentNume('eeee1111'), 'Carolyn Shaw')

        self.__catalog.modifyStudentNume('ffff1111', 'Keaton Rasmussen')
        self.assertEqual(self.__catalog.getStudentNume('ffff1111'), 'Keaton Rasmussen')


    def testModifyStudentGrupa(self):
        self.__catalog.modifyStudentGrupa('dddd1111', 234)
        self.assertEqual(self.__catalog.getStudentGrupa('dddd1111'), 234)

        self.__catalog.modifyStudentGrupa('eeee1111', 233)
        self.assertEqual(self.__catalog.getStudentGrupa('eeee1111'), 233)

        self.__catalog.modifyStudentGrupa('ffff1111', 232)
        self.assertEqual(self.__catalog.getStudentGrupa('ffff1111'), 232)


    def testDeleteStudent(self):
        self.__catalog.deleteStudent('dddd1111')
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentNume('dddd1111'))
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentGrupa('dddd1111'))

        self.__catalog.deleteStudent('eeee1111')
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentNume('eeee1111'))
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentGrupa('eeee1111'))

        self.__catalog.deleteStudent('ffff1111')
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentNume('ffff1111'))
        self.assertRaises(KeyError, lambda: self.__catalog.getStudentGrupa('ffff1111'))


    def testGetStudent(self):
        self.assertEqual(self.__catalog.getStudent('dddd1111'), ['Mathias Little', 222])
        self.assertEqual(self.__catalog.getStudent('eeee1111'), ['Lia Shelton', 232])
        self.assertEqual(self.__catalog.getStudent('ffff1111'), ['Jonathon Sheppard', 212])


    def testGetAll(self):
        self.assertEqual(self.__catalog.getAll(), {'dddd1111': ['Mathias Little', 222],
                                                   'eeee1111': ['Lia Shelton', 232],
                                                   'ffff1111': ['Jonathon Sheppard', 212]})

    if __name__ == '__main__':
        unittest.main()


class TestStudent(unittest.TestCase):
    def setUp(self):
        self.__studentList = []
        self.__studentList.append(Student('Waylon Dalton', 213))
        self.__studentList.append(Student('Marcus Cruz', 214))
        self.__studentList.append(Student('Eddie Randolph', 215))


    def testGetNume(self):
        self.assertEqual(self.__studentList[0].getNume(), 'Waylon Dalton')
        self.assertEqual(self.__studentList[1].getNume(), 'Marcus Cruz')
        self.assertEqual(self.__studentList[2].getNume(), 'Eddie Randolph')


    def testGetGrupa(self):
        self.assertEqual(self.__studentList[0].getGrupa(), 213)
        self.assertEqual(self.__studentList[1].getGrupa(), 214)
        self.assertEqual(self.__studentList[2].getGrupa(), 215)


    def testModifyName(self):
        self.__studentList[0].modifyName('Hadassah Hartman'),
        self.assertEqual(self.__studentList[0].getNume(), 'Hadassah Hartman')

        self.__studentList[1].modifyName('Justine Henderson'),
        self.assertEqual(self.__studentList[1].getNume(), 'Justine Henderson')

        self.__studentList[2].modifyName('Thalia Cobb'),
        self.assertEqual(self.__studentList[2].getNume(), 'Thalia Cobb')


    def testModifyGrupa(self):
        self.__studentList[0].modifyGrupa(211)
        self.assertEqual(self.__studentList[0].getGrupa(), 211)

        self.__studentList[1].modifyGrupa(215)
        self.assertEqual(self.__studentList[1].getGrupa(), 215)

        self.__studentList[2].modifyGrupa(216)
        self.assertEqual(self.__studentList[2].getGrupa(), 216)


    if __name__ == '__main__':
        unittest.main()

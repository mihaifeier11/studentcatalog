import unittest
import datetime
from domain.lab import ProblemaLab, Probleme

class TestLabDomain(unittest.TestCase):
    def setUp(self):
        self.__problemaLab = ProblemaLab('Descriere', datetime.date(day = 10, month = 10, year = 2017))
        self.__probleme = Probleme()
        self.__probleme.addProblema('2_1', 'Descriere 2_1', datetime.date(day = 10, month = 10, year = 2017))
        self.__probleme.addProblema('3_1', 'Descriere 3_1', datetime.date(day = 17, month = 10, year = 2017))
        self.__probleme.addProblema('4_1', 'Descriere 4_1', datetime.date(day = 24, month = 10, year = 2017))


    def testGetDescriere(self):
        self.assertEqual(self.__problemaLab.getDescriere(), 'Descriere')


    def testGetDeadline(self):
        self.assertEqual(self.__problemaLab.getDeadline(), datetime.date(day = 10, month = 10, year = 2017))


    def testModifyDescriere(self):
        self.__problemaLab.modifyDescriere('Descriere 1')
        self.assertEqual(self.__problemaLab.getDescriere(), 'Descriere 1')

        self.__problemaLab.modifyDescriere('Descriere 2')
        self.assertEqual(self.__problemaLab.getDescriere(), 'Descriere 2')

        self.__problemaLab.modifyDescriere('Descriere 3')
        self.assertEqual(self.__problemaLab.getDescriere(), 'Descriere 3')


    def testModifyDeadline(self):
        self.__problemaLab.modifyDeadline(datetime.date(day = 11, month = 10, year = 2017))
        self.assertEqual(self.__problemaLab.getDeadline(), datetime.date(day = 11, month = 10, year = 2017))

        self.__problemaLab.modifyDeadline(datetime.date(day = 11, month = 12, year = 2017))
        self.assertEqual(self.__problemaLab.getDeadline(), datetime.date(day = 11, month = 12, year = 2017))

        self.__problemaLab.modifyDeadline(datetime.date(day = 11, month = 10, year = 2018))
        self.assertEqual(self.__problemaLab.getDeadline(), datetime.date(day = 11, month = 10, year = 2018))


    def testAddProblema(self):
        self.__probleme.addProblema('1_1', 'Descriere', datetime.date(day = 10, month = 10, year = 2017))
        self.assertEqual(self.__probleme.getProblemaDescriere('1_1'), 'Descriere')
        self.assertEqual(self.__probleme.getProblemaDeadline('1_1'), datetime.date(day = 10, month = 10, year = 2017))

        self.__probleme.addProblema('2_2', 'Descriere 2', datetime.date(day = 11, month = 10, year = 2017))
        self.assertEqual(self.__probleme.getProblemaDescriere('2_2'), 'Descriere 2')
        self.assertEqual(self.__probleme.getProblemaDeadline('2_2'), datetime.date(day = 11, month = 10, year = 2017))

        self.__probleme.addProblema('3_3', 'Descriere 3', datetime.date(day = 10, month = 10, year = 2018))
        self.assertEqual(self.__probleme.getProblemaDescriere('3_3'), 'Descriere 3')
        self.assertEqual(self.__probleme.getProblemaDeadline('3_3'), datetime.date(day = 10, month = 10, year = 2018))


    def testGetLabDescriere(self):
        self.assertEqual(self.__probleme.getProblemaDescriere('2_1'), 'Descriere 2_1')
        self.assertEqual(self.__probleme.getProblemaDescriere('3_1'), 'Descriere 3_1')
        self.assertEqual(self.__probleme.getProblemaDescriere('4_1'), 'Descriere 4_1')


    def testGetLabDeadline(self):
        self.assertEqual(self.__probleme.getProblemaDeadline('2_1'), datetime.date(day = 10, month = 10, year = 2017))
        self.assertEqual(self.__probleme.getProblemaDeadline('3_1'), datetime.date(day = 17, month = 10, year = 2017))
        self.assertEqual(self.__probleme.getProblemaDeadline('4_1'), datetime.date(day = 24, month = 10, year = 2017))


    def testModifyProblemaNr(self):
        self.__probleme.modifyProblemaNr('2_1', '2_2')
        self.assertEqual(self.__probleme.getProblemaDescriere('2_2'), 'Descriere 2_1')
        self.assertEqual(self.__probleme.getProblemaDeadline('2_2'), datetime.date(day = 10, month = 10, year = 2017))
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDescriere('2_1'))
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDeadline('2_1'))


        self.__probleme.modifyProblemaNr('3_1', '3_2')
        self.assertEqual(self.__probleme.getProblemaDescriere('3_2'), 'Descriere 3_1')
        self.assertEqual(self.__probleme.getProblemaDeadline('3_2'), datetime.date(day = 17, month = 10, year = 2017))
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDescriere('3_1'))
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDeadline('3_1'))

        self.__probleme.modifyProblemaNr('4_1', '4_2')
        self.assertEqual(self.__probleme.getProblemaDescriere('4_2'), 'Descriere 4_1')
        self.assertEqual(self.__probleme.getProblemaDeadline('4_2'), datetime.date(day = 24, month = 10, year = 2017))
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDescriere('4_1'))
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDeadline('4_1'))


    def testModifyProblemaDescriere(self):
        self.__probleme.modifyProblemaDescriere('2_1', 'Descriere modificata 2_1')
        self.assertEqual(self.__probleme.getProblemaDescriere('2_1'), 'Descriere modificata 2_1')

        self.__probleme.modifyProblemaDescriere('3_1', 'Descriere modificata 3_1')
        self.assertEqual(self.__probleme.getProblemaDescriere('3_1'), 'Descriere modificata 3_1')

        self.__probleme.modifyProblemaDescriere('4_1', 'Descriere modificata 4_1')
        self.assertEqual(self.__probleme.getProblemaDescriere('4_1'), 'Descriere modificata 4_1')


    def testModifyProblemaDeadline(self):
        self.__probleme.modifyProblemaDeadline('2_1', datetime.date(day = 10, month = 11, year = 2017))
        self.assertEqual(self.__probleme.getProblemaDeadline('2_1'), datetime.date(day = 10, month = 11, year = 2017))

        self.__probleme.modifyProblemaDeadline('3_1', datetime.date(day = 10, month = 12, year = 2017))
        self.assertEqual(self.__probleme.getProblemaDeadline('3_1'), datetime.date(day = 10, month = 12, year = 2017))

        self.__probleme.modifyProblemaDeadline('4_1', datetime.date(day = 10, month = 10, year = 2017))
        self.assertEqual(self.__probleme.getProblemaDeadline('4_1'), datetime.date(day = 10, month = 10, year = 2017))


    def testDeleteProblema(self):
        self.__probleme.deleteProblema('2_1')
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDescriere('2_1'))
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDeadline('2_1'))

        self.__probleme.deleteProblema('3_1')
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDescriere('3_1'))
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDeadline('3_1'))

        self.__probleme.deleteProblema('4_1')
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDescriere('4_1'))
        self.assertRaises(KeyError, lambda: self.__probleme.getProblemaDeadline('4_1'))


    def testGetProblema(self):
        self.assertEqual(self.__probleme.getProblema('2_1'), ['Descriere 2_1', '2017-10-10'])
        self.assertEqual(self.__probleme.getProblema('3_1'), ['Descriere 3_1', '2017-10-17'])
        self.assertEqual(self.__probleme.getProblema('4_1'), ['Descriere 4_1', '2017-10-24'])


    def testGetAll(self):
        self.assertEqual(self.__probleme.getAll(), {'2_1': ['Descriere 2_1', '2017-10-10'],
                                                    '3_1': ['Descriere 3_1', '2017-10-17'],
                                                    '4_1': ['Descriere 4_1', '2017-10-24']})



if __name__ == '__main__':
    unittest.main()

import unittest
from controllers.controller import GradeController

class testGradeController(unittest.TestCase):
    def setUp(self):
        self.__gradeController = GradeController()


    def testSortByName(self):
        lista = [['D', 10], ['C', 2], ['D', 3], ['A', 4]]
        self.assertEqual([['A', 4], ['C', 2], ['D', 10], ['D', 3]], self.__gradeController.sortByName(lista))

        lista = [['B', 3], ['Q', 10], ['G', 7], ['O', 4]]
        self.assertEqual([['B', 3], ['G', 7], ['O', 4], ['Q', 10]], self.__gradeController.sortByName(lista))

        lista = [['Y', 5], ['U', 6], ['I', 8], ['T', 4]]
        self.assertEqual([['I', 8], ['T', 4], ['U', 6], ['Y', 5]], self.__gradeController.sortByName(lista))


    def testSortByGrade(self):
        lista = [['D', 10], ['C', 2], ['D', 3], ['A', 4]]

        self.assertEqual([['C', 2], ['D', 3], ['A', 4], ['D', 10]], self.__gradeController.sortByGrade(lista))

        lista = [['B', 3], ['Q', 10], ['G', 7], ['O', 4]]

        self.assertEqual([['B', 3], ['O', 4], ['G', 7], ['Q', 10]], self.__gradeController.sortByGrade(lista))

        lista = [['Y', 5], ['U', 6], ['I', 8], ['T', 4]]

        self.assertEqual([['T', 4], ['Y', 5], ['U', 6], ['I', 8]], self.__gradeController.sortByGrade(lista))


    def testMedie(self):
        lista = {'A': 10, 'B': 8, 'C': 6, 'D': 5}
        self.assertEqual(7.25, self.__gradeController.medie(lista))

        lista = {'A': 1, 'B': 6, 'C': 6, 'D': 5}
        self.assertEqual(4.5, self.__gradeController.medie(lista))

        lista = {'A': 10, 'B': 8, 'C': 7, 'D': 5}
        self.assertEqual(7.5, self.__gradeController.medie(lista))


if __name__ == '__main__':
    unittest.main()

from domain.student import Catalog
from domain.lab import Probleme
from domain.grades import GradeList

from repositories.studentRepository import StudentRepo, StudentRepositoryError
from repositories.labRepository import LabRepo, LabRepositoryError
from repositories.gradeRepository import GradeRepo, GradeRepositoryError
from repositories.problemRepository import ProblemRepo

from validators.studentValidators import StudentError, StudentValidators
from validators.problemValidators import ProblemValidators, LabError
from validators.gradeValidators import GradeError, GradeValidators

from controllers.controller import StudentController, ProblemController, ControllerError, GradeController
import copy


class UndoError(Exception):
    pass


class UI:
    '''
        adaugaStudent(instructiune) - functia prelucreaza instructiunea, apeleaza functia de adaugare student si
        updateaza repository-ul

        help() - printeaza lista comenzilor disponibile

        modificaNrMatricolStudent(instructiune) - functia prelucreaza instructiunea, apeleaza functia de modificare a
        numarului matricol al unui student si updateaza repository-ul

        modificaNumeStudent(instructiune) - functia prelucreaza instructiunea, apeleaza functia de modificare a
        numelui unui student si updateaza repository-ul

        modificaGrupaStudent(instructiune) - functia prelucreaza instructiunea, apeleaza functia de modificare a
        grupei al unui student si updateaza repository-ul

        stergeStudent(instructiune) - functia prelucreaza instructiunea, apeleaza functia de stergere a unui student
        si updateaza repository-ul

        tiparireStudent(instructiune) - functia prelucreaza instrictiunea si tipareste studentul cu numarul matricol dat

        tiparireStudenti(instructiune) - functia tipareste toti studentii

        adaugaProblema(instructiune) - functia prelucreaza instructiunea, apeleaza functia de adaugare a problemei si
        updateaza repository-ul

        modificaNrProblema(instructiune) - functia prelucreaza instructiunea, apeleaza functia de modificarea a numarului
        problemei si updateaza repository-ul

        modificaDescriereProblema(instructiune) - functia prelucreaza instructiunea, apeleaza functia de modificarea a
        descrierii problemei si updateaza repository-ul

        modificaDeadlineProblema(instructiune) - functia prelucreaza instructiunea, apeleaza functia de modificarea a
        deadline-ului problemei si updateaza repository-ul

        stergeProblema(instructiune) - functia prelucreaza instructiunea, apeleaza functia de stergere a problemei, atat
        din lista de probleme, cat si din lista de probleme asignate si updateaza repository-ul

        tiparireProblema(instructiune) - functia prelucreaza instructiunea si tipareste problema cu numarul dat

        tiparireProbleme(instructiune) - functia tipareste toate problemele

        undo() - reface ultima operatie

        adaugaNota(instructiune) - adauga o nota unui student

        asignareProblema(instructiune) - asigneaza o problema unui student

        modificaNota(instructiune) - modifica nota unui student la un laborator dat

        stergeProblemaAsignata(instructiune) - sterge o problema din lista problemelor asignate

        tiparireProblemeAsignate(instructiune) - tipareste toate problemele asignate unui student

        tiparireNoteStudent(instructiune) - tipareste toate notele unui student

        tiparireStudentiOrdinatiAlfabetic() - tipareste studentii ordonati alfabetic

        tiparireStudentiOrdinatiDupaNota() - tipareste studentii orodnati dupa medie

        tiparireStudentiNoteMaiMici5() - tipareste toti studentii cu media notelor mai mica decat 5

        help() - tipareste lista instructiunilor disponibile

        UiFileRepo() - functia de UI, aceasta citeste si prelucreaza comanda
    '''
    def __init__(self):
        self.__catalog = Catalog()
        self.__probleme = Probleme()
        self.__note = GradeList()

        self.__studentRepo = StudentRepo('./repositories/students.txt', self.__catalog)
        self.__labRepo = LabRepo('./repositories/lab.txt', self.__probleme)
        self.__gradeRepo = GradeRepo('./repositories/grades.txt', self.__note)
        self.__problemRepo = ProblemRepo('./repositories/problems.txt', self.__note)

        self.__studentValidators = StudentValidators()
        self.__problemValidators = ProblemValidators()
        self.__gradeValidators = GradeValidators()

        self.__backupListCommands = []

        self.__backupListStudent = []
        self.__backupListStudent.append(copy.deepcopy(self.__catalog))

        self.__backupListLab = []
        self.__backupListLab.append(copy.deepcopy(self.__probleme))

        self.__backupListGrades = []
        self.__backupListGrades.append(copy.deepcopy(self.__note))

        self.__studentController = StudentController()
        self.__problemController = ProblemController()
        self.__gradeController = GradeController()


    def adaugaStudent(self, instructiune):
        nrMatricol = instructiune[0]
        self.__studentValidators.validateNewNrMatricol(nrMatricol, self.__catalog.getAll())

        nume = instructiune[1]
        self.__studentValidators.validateNume(nume)

        grupa = instructiune[2]
        self.__studentValidators.validateGrupa(grupa)

        self.__catalog.addStudent(nrMatricol, nume, grupa)
        self.__studentRepo.update(self.__catalog)
        self.__backupListStudent.append(copy.deepcopy(self.__catalog))
        self.__backupListCommands.append('s')
        print ('Studentul', nume, 'a fost adaugat cu succes.')


    def modificaNrMatricolStudent(self, instructiune):
        nrMatricol = instructiune[0]
        self.__studentValidators.validateNrMatricol(nrMatricol, self.__catalog.getAll())

        newNrMatricol = instructiune[1]
        self.__studentValidators.validateNewNrMatricol(newNrMatricol, self.__catalog.getAll())

        self.__catalog.modifyStudentNrMatricol(nrMatricol, newNrMatricol)
        self.__studentRepo.update(self.__catalog)
        self.__backupListStudent.append(copy.deepcopy(self.__catalog))
        self.__backupListCommands.append('s')
        print ('Numarul matricol a fost modificat din', nrMatricol, 'in', newNrMatricol, 'cu succes.')


    def modificaNumeStudent(self, instructiune):
        nrMatricol = instructiune[0]
        self.__studentValidators.validateNrMatricol(nrMatricol, self.__catalog.getAll())

        nume = instructiune[1]
        self.__studentValidators.validateNume(nume)

        self.__catalog.modifyStudentNume(nrMatricol, nume)
        self.__studentRepo.update(self.__catalog)

        self.__backupListStudent.append(copy.deepcopy(self.__catalog))
        self.__backupListCommands.append('s')

        print ('Numele studentului cu numarul matricol', nrMatricol, 'a fost modificat in', nume, 'cu succes.')


    def modificaGrupaStudent(self, instructiune):
        nrMatricol = instructiune[0]
        self.__studentValidators.validateNrMatricol(nrMatricol, self.__catalog.getAll())

        grupa = instructiune[1]
        self.__studentValidators.validateGrupa(grupa)

        self.__catalog.modifyStudentGrupa(nrMatricol, grupa)
        self.__studentRepo.update(self.__catalog)
        self.__backupListStudent.append(copy.deepcopy(self.__catalog))
        self.__backupListCommands.append('s')
        print ('Grupa studentului cu numarul matricol', nrMatricol, 'a fost modificata in', grupa, 'cu succes.')


    def stergeStudent(self, instructiune):
        nrMatricol = instructiune[0]
        self.__studentValidators.validateNrMatricol(nrMatricol, self.__catalog.getAll())

        self.__catalog.deleteStudent(nrMatricol)
        self.__studentRepo.update(self.__catalog)
        self.__backupListStudent.append(copy.deepcopy(self.__catalog))
        self.__backupListCommands.append('s')
        print ('Studentul cu numarul matricol', nrMatricol, 'a fost sters cu succes.')


    def tiparireStudent(self, instructiune):
        nrMatricol = instructiune[0]
        self.__studentValidators.validateNrMatricol(nrMatricol, self.__catalog.getAll())

        dateStudent = self.__catalog.getStudent(nrMatricol)
        print ('Nume:', dateStudent[0])
        print ('Grupa:', dateStudent[1])


    def tiparireStudenti(self, instructiune):
        dateStudenti = self.__catalog.getAll()
        for elem in dateStudenti:
           print ('{:^12}'.format(elem) + '{:^21}'.format(dateStudenti[elem][0]) + '{:^5}'.format(dateStudenti[elem][1]))


    def adaugaProblema(self, instructiune):
        nrProblema = instructiune[0]
        self.__problemValidators.validateNewNrProblema(nrProblema, self.__probleme.getAll())

        descriere = instructiune[1]

        deadline = instructiune[2]
        self.__problemValidators.validateDeadline(deadline)

        self.__probleme.addProblema(nrProblema, descriere, deadline)
        self.__labRepo.update(self.__probleme)

        self.__backupListLab.append(copy.deepcopy(self.__probleme))
        self.__backupListCommands.append('p')
        print ('Problema cu numarul', nrProblema, 'a fost adaugata cu succes.')


    def modificaNrProblema(self, instructiune):
        nrProblema = instructiune[0]
        self.__problemValidators.validateNrProblema(nrProblema, self.__probleme.getAll())

        newNrProblema = instructiune[1]
        self.__problemValidators.validateNewNrProblema(newNrProblema, self.__probleme.getAll())

        self.__probleme.modifyProblemaNr(nrProblema, newNrProblema)
        self.__labRepo.update(self.__probleme)
        self.__backupListLab.append(copy.deepcopy(self.__probleme))
        self.__backupListCommands.append('p')
        print ('Numarul problemei a fost modificat din', nrProblema, 'in', newNrProblema, 'cu succes.')


    def modificaDescriereProblema(self, instructiune):
        nrProblema = instructiune[0]
        self.__problemValidators.validateNrProblema(nrProblema, self.__probleme.getAll())

        descriere = instructiune[1]

        self.__probleme.modifyProblemaDescriere(nrProblema, descriere)
        self.__labRepo.update(self.__probleme)
        self.__backupListLab.append(copy.deepcopy(self.__probleme))
        self.__backupListCommands.append('p')
        print ('Descrierea problemei cu numarul', nrProblema, 'a fost modificata cu succes.')


    def modificaDeadlineProblema(self, instructiune):
        nrProblema = instructiune[0]
        self.__problemValidators.validateNrProblema(nrProblema, self.__probleme.getAll())

        deadline = instructiune[2]
        self.__problemValidators.validateDeadline(deadline)

        self.__probleme.modifyProblemaDeadline(nrProblema, deadline)
        self.__labRepo.update(self.__probleme)
        self.__backupListLab.append(copy.deepcopy(self.__probleme))
        self.__backupListCommands.append('p')
        print ('Deadline-ul problemei cu numarul', nrProblema, 'a fost modificat in', deadline, 'cu succes.')


    def stergeProblema(self, instructiune):
        nrProblema = instructiune[0]
        self.__problemValidators.validateNrProblema(nrProblema, self.__probleme.getAll())

        self.__probleme.deleteProblema(nrProblema)
        self.__labRepo.update(self.__probleme)
        self.__backupListLab.append(copy.deepcopy(self.__probleme))
        self.__backupListCommands.append('p')
        print ('Problema cu numarul', nrProblema, 'a fost stearsa cu succes.')


    def tiparireProblema(self, instructiune):
        nrProblema = instructiune[0]
        self.__problemValidators.validateNrProblema(nrProblema, self.__probleme.getAll())

        problema = self.__probleme.getProblema(nrProblema)

        print ('Descriere:', problema[0])
        print ('Deadline:', problema[1])


    def tiparireProbleme(self, instructiune):
        probleme = self.__probleme.getAll()
        for elem in probleme:
            print ('Numarul problemei:', elem)
            print ('Descriere:', probleme[elem][0])
            print ('Deadline:', probleme[elem][1])


    def undo(self, instructiune):
        if len(self.__backupListCommands) == 0:
            raise UndoError('Operatia nu poate fi efectuata.')

        if self.__backupListCommands[-1] == 's':
            del self.__backupListStudent[len(self.__backupListStudent)-1]
            del self.__backupListCommands[-1]
            self.__catalog = copy.deepcopy(self.__backupListStudent[len(self.__backupListStudent)-1])
            self.__studentRepo.update(self.__catalog)

        elif self.__backupListCommands[-1] == 'p':
            del self.__backupListLab[-1]
            del self.__backupListCommands[-1]
            self.__probleme = copy.deepcopy((self.__backupListLab[-1]))
            self.__labRepo.update(self.__probleme)

        elif self.__backupListCommands[-1] == 'g':
            del self.__backupListGrades[-1]
            del self.__backupListCommands[-1]
            self.__note = copy.deepcopy(self.__backupListGrades[-1])
            self.__gradeRepo.update(self.__note)


    def adaugaNota(self, instructiune):
        nrMatricol = instructiune[0]
        nrProblema = instructiune[1]
        nota = instructiune[2]

        self.__studentValidators.validateNrMatricol(nrMatricol, self.__catalog.getAll())
        self.__problemValidators.validateNrProblema(nrProblema, self.__probleme.getAll())
        self.__gradeValidators.validateGrade(nota)

        nota = int(nota)

        self.__note.addGrade(nrMatricol, nrProblema, nota)

        self.asignareProblema(instructiune)
        self.__gradeRepo.update(self.__note)
        self.__backupListGrades.append(self.__note)
        self.__backupListCommands.append('g')


    def asignareProblema(self, instructiune):
        nrMatricol = instructiune[0]
        nrProblema = instructiune[1]

        self.__studentValidators.validateNrMatricol(nrMatricol, self.__catalog.getAll())
        self.__problemValidators.validateNrProblema(nrProblema, self.__probleme.getAll())

        self.__note.assignGrade(nrMatricol, nrProblema)

        self.__problemRepo.update(self.__note)
        self.__backupListGrades.append(self.__note)
        self.__backupListCommands.append('g')


    def modificaNota(self, instructiune):
        nrMatricol = instructiune[0]
        nrProblema = instructiune[1]
        nota = instructiune[2]

        self.__studentValidators.validateNrMatricol(nrMatricol, self.__catalog.getAll())
        self.__problemValidators.validateNrProblema(nrProblema, self.__probleme.getAll())
        self.__gradeValidators.validateGrade(nota)

        nota = int(nota)

        self.__note.modifyGrade(nrMatricol, nrProblema, nota)
        self.__backupListGrades.append(self.__note)
        self.__backupListCommands.append('g')


    def stergeProblemaAsignata(self, instructiune):
        nrMatricol = instructiune[0]
        nrProblema = instructiune[1]

        self.__studentValidators.validateNrMatricol(nrMatricol, self.__catalog.getAll())
        self.__problemValidators.validateNrProblema(nrProblema, self.__probleme.getAll())

        self.__note.removeProblem(nrMatricol, nrProblema)
        self.__backupListGrades.append(self.__note)
        self.__backupListCommands.append('g')


    def tiparireProblemeAsignate(self, instructiune):
        nrMatricol = instructiune[0]
        self.__studentValidators.validateNrMatricol(nrMatricol, self.__catalog.getAll())

        print ('Problemele asignate studentului cu numarul matricol', nrMatricol, 'sunt:', end = ' ')

        problemeAsignate = self.__note.getAssignedProblems(nrMatricol)

        for i in range(len(problemeAsignate)-1):
            print (problemeAsignate[i], end = ', ')

        if (len(problemeAsignate) > 0):
            print (problemeAsignate[-1])


    def tiparireNoteStudent(self, instructiune):
        nrMatricol = instructiune[0]
        self.__studentValidators.validateNrMatricol(nrMatricol, self.__catalog.getAll())

        noteStudent = self.__note.getGrades(nrMatricol)

        print ('Notele studentului cu numarul matricol', nrMatricol, 'sunt:', end = ' ')

        for elem in noteStudent:
            print (elem + ': ' + str(noteStudent[elem]), end = ', ')

        print ('\n')


    def tiparireStudentiOrdonatiAlfabetic(self, instructiune):
        nrProblema = instructiune[0]
        self.__problemValidators.validateNrProblema(nrProblema, self.__probleme.getAll())
        listaNote = []
        listaStudenti = self.__catalog.getAll()
        for elem in listaStudenti:
            if self.__note.hasProblemAssigned(elem, nrProblema) == True:
                listaNote.append([listaStudenti[elem][0], self.__note.getGrade(elem, nrProblema)])
        listaNote = self.__gradeController.sortByName(listaNote)
        for i in range (len(listaNote)):
            print(listaNote[i][0], end=': ')
            print(listaNote[i][1])


    def tiparireStudentiOrdonatiDupaNota(self, instructiune):
        nrProblema = instructiune[0]
        self.__problemValidators.validateNrProblema(nrProblema, self.__probleme.getAll())
        listaNote = []
        listaStudenti = self.__catalog.getAll()
        for elem in listaStudenti:
            if self.__note.hasProblemAssigned(elem, nrProblema) == True:
                listaNote.append([listaStudenti[elem][0], self.__note.getGrade(elem, nrProblema)])

        listaNote = self.__gradeController.sortByGrade(listaNote)
        for i in range (len(listaNote)):
            print(listaNote[i][0], end=': ')
            print(listaNote[i][1])


    def tiparireStudentiNoteMaiMici5(self, instructiune):
        listaStudenti = self.__catalog.getAll()
        for elem in listaStudenti:
            if self.__note.hasGrades(elem) == True:
                lista = self.__note.getGrades(elem)
                gradeList = []
                for problem in lista:
                    gradeList.append(lista[problem])

                nota = self.__gradeController.medie(gradeList)
                if nota < 5:
                    print (listaStudenti[elem][0], end = ': ')
                    print (nota)


    def tiparireStudentiNotePeste5(self, instructiune):
        listaStudenti = self.__catalog.getAll()
        for elem in listaStudenti:
            if self.__note.hasGrades(elem) == True:
                lista = self.__note.getGrades(elem)
                gradeList = []
                for problem in lista:
                    gradeList.append(lista[problem])

                nota = self.__gradeController.medie(gradeList)
                if nota >= 5:
                    print (listaStudenti[elem][0], end = ': ')
                    print (nota)


    def adaugareStudentRandom(self, instructiune):
        numar = int(instructiune[0])
        for i in range (numar):
            command = []
            command.append(self.__studentController.randomId(self.__catalog.getAll()))
            command.append(self.__studentController.randomName())
            command.append(self.__studentController.randomGroup())
            self.adaugaStudent(command)


    def adaugareProblemaRandom(self, instructiune):
        numar = int(instructiune[0])

        for i in range (numar):
            command = []
            command.append(self.__problemController.randomId(self.__probleme.getAll()))
            command.append(self.__problemController.randomDescription())
            command.append(self.__problemController.randomDate())
            self.adaugaProblema(command)


    def asignareProblemaRandom(self, instructiune):
        numar = int(instructiune[0])
        for i in range (numar):
            self.asignareProblema(self.__gradeController.randomGrade(self.__catalog.getAll(), self.__probleme.getAll()))


    def adaugaNotaRandom(self, instructiune):
        numar = int(instructiune[0])
        for i in range (numar):
            self.adaugaNota(self.__gradeController.randomGrade(self.__catalog.getAll(), self.__probleme.getAll()))


    def help(self):
        print ('Pentru a adauga un student, scrieti: adauga_student <nr_matricol> <nume_prenume> <grupa>')
        print ('Pentru a modifica numarul matricol al unui student, scrieti: modifica_student_nrmatricol <nr_matricol> <noul_nr_matricol')
        print ('Pentru a modifica numele unui student, scrieti: modifica_student_nume <nr_matricol> <nume>')
        print ('Pentru a modifica grupa unui student, scrieti: modifica_student_grupa <nr_matricol> <grupa>')
        print ('Pentru a sterge un student, scrieti: sterge_student <nr_matricol>')
        print ('Pentru a tipari datele unui student, scrieti: tiparire_student <nr_matricol>')
        print ('Pentru a tipari datele tuturor studentilor, scrieti: tiparire_studenti')

        print ('Pentru a adauga o problema, scrieti: adauga_problema <nr_problema>/ <descriere>/ <deadline(aa-ll-zz>')
        print ('Pentru a modifica numarul unei probleme, scrieti: modifica_problema_nr <nr_problema>/ <noul_nr_problema>')
        print ('Pentru a modifica descrierea unei probleme, scrieti: modifica_problema_descriere <nr_problema>/ <descriere>')
        print ('Pentru a modifica deadline-ul unei probleme, scrieti: modifica_problema_deadline <nr_problema>/ <deadline>')
        print ('Pentru a sterge o problema, scrieti: sterge_problema <nr_problema>')
        print ('Pentru a tipari datele unei probleme, scrieti: tiparire_problema <nr_matricol>')
        print ('Pentru a tipari datele tuturor problemelor, scrieti: tiparire_probleme')

        print ('Pentru a adauga o nota unui student, scrieti: adauga_nota <nr_matricol>/ <nr_problema>/ <nota>')
        print ('Pentru a tipari toate notele unui student, scrieti: tiparire_note_student <nr_matricol>')
        print ('Pentru a asigna o problema unui student, scrieti: asignare_problema <nr_matricol>/ <nr_problema>')
        print ('Pentru a adauga o nota, scrieti: adauga_nota: <nr_matricol>/ <nr_problema>/ <nota>')
        print ('Pentru a sterge o problema asignata, scrieti: sterge_problema_asignata <nr_matricol>/ <nr_problema>')
        print ('Pentru a tipari problemele asignate, scrieti tiparire_probleme_asignate <nr_matricol>')
        print ('Pentru a tipari notele unui student, scrieti: tiparire_note <nr_matricol>')
        print ('Pentru a tipari studentii sortati alfabetic in functie de o problema, scrieti: tiparire_studenti_sortati_alfabetic <nr_problema>')
        print ('Pentru a tipari studentii sortati in functie de nota la o problema, scrieti: tiparire_studenti_sortati_alfabetic <nr_problema>')
        print ('Pentru a tipari studentii cu media sub 5, scrieti: tiparire_studenti_medie_sub_5')
        print ('Pentru a tipari studentii cu media peste 5, scrieti: tiparire_studenti_medie_peste_5')
        print ('Pentru a adauga un numar random de studenti, scrieti: adauga_student_random <nr_studenti>')
        print ('Pentru a adauga un numar random de probleme, scrieti: adauga_problema_random <nr_probleme>')
        print ('Pentru a asigna un numar de probleme random, scrieti: asignare_problema_random <nr_probleme>')
        print ('Pentru a adauga un numar de note random, scrieti: adauga_nota_random <nr_note>')
        print ('s')


    def UiFileRepo(self):
        commandList = {'adauga_student': self.adaugaStudent,
                       'modifica_student_nrmatricol': self.modificaNrMatricolStudent,
                       'modifica_student_nume': self.modificaNumeStudent,
                       'modifica_student_grupa': self.modificaGrupaStudent,
                       'sterge_student': self.stergeStudent,
                       'tiparire_student': self.tiparireStudent,
                       'tiparire_studenti': self.tiparireStudenti,
                       'adauga_problema': self.adaugaProblema,
                       'modifica_problema_nr': self.modificaNrProblema,
                       'modifica_problema_descriere': self.modificaDescriereProblema,
                       'modifica_problema_deadline': self.modificaDeadlineProblema,
                       'sterge_problema': self.stergeProblema,
                       'tiparire_problema': self.tiparireProblema,
                       'tiparire_probleme': self.tiparireProbleme,
                       'asignare_problema': self.asignareProblema,
                       'adauga_nota': self.adaugaNota,
                       'sterge_problema_asignata': self.stergeProblemaAsignata,
                       'tiparire_probleme_asignate': self.tiparireProblemeAsignate,
                       'tiparire_note_student': self.tiparireNoteStudent,
                       'tiparire_studenti_sortati_alfabetic': self.tiparireStudentiOrdonatiAlfabetic,
                       'tiparire_studenti_sortati_nota': self.tiparireStudentiOrdonatiDupaNota,
                       'tiparire_studenti_medie_sub_5': self.tiparireStudentiNoteMaiMici5,
                       'tiparire_studenti_medie_peste_5': self.tiparireStudentiNotePeste5,
                       'adauga_student_random': self.adaugareStudentRandom,
                       'adauga_problema_random': self.adaugareProblemaRandom,
                       'asignare_problema_random': self.asignareProblemaRandom,
                       'adauga_nota_random': self.adaugaNotaRandom,
                       'undo': self.undo}

        while True:
            try:
                print('Pentru a vedea comenzile disponibile, scrieti "help".')
                stController = StudentController()
                command = input('Comanda: ')
                command = command.split(' ')
                comanda = ' '.join(command[1:])
                comanda = comanda.split('/')
                if command[0] == 'help':
                    self.help()

                elif command[0] == 'iesire':
                    exit('Va dorim o zi buna.')

                else:
                    commandList[command[0]](comanda)
            except StudentError as msg:
                print (msg)
            except StudentRepositoryError as msg:
                print (msg)
            except LabRepositoryError as msg:
                print (msg)
            except LabError as msg:
                print (msg)
            except ControllerError as msg:
                print (msg)
            except UndoError as msg:
                print (msg)

            # except BaseException:
            #     print ('Structura comenzii este incorecta.')


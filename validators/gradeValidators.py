class GradeError(Exception):
    pass

class GradeValidators:
    def __init__(self):
        pass

    def validateGrade(self, grade):
        if grade.isdigit() == False or int(grade) < 0 or int(grade) > 10:
            raise GradeError('Nota introdusa nu este valida. Aceasta trebuie sa fie intre 1 si 10.')


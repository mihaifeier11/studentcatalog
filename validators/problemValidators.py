import unittest
import datetime
class LabError(Exception):
    pass

class ProblemValidators(unittest.TestCase):

    def validateNrProblema(self, nrProblema, prList):
        nrPr = nrProblema.split('_')
        if nrPr[0].isdigit() == False or nrPr[1].isdigit() == False or len(nrPr) != 2:
            raise LabError('Numarul problemei este introdus gresit.')
        exist = False
        for elem in prList:
            if elem == nrProblema:
                exist = True

        if not exist:
            raise LabError('Problema cu numarul introdus nu exista.')

    def validateNewNrProblema(self, nrProblema, prList):
        nrPr = nrProblema.split('_')
        if nrPr[0].isdigit() == False or nrPr[1].isdigit() == False or len(nrPr) != 2:
            raise LabError('Numarul problemei este introdus gresit.')
        exist = False
        for elem in prList:
            if elem == nrProblema:
                exist = True

        if exist:
            raise LabError('Problema cu numarul introdus exista deja.')

    def validateDeadline(self, deadline):
        date = deadline.split('-')
        try:
            date = datetime.date(year = int(date[0]), month = int(date[1]), day = int(date[2]))
        except ValueError:
            raise LabError('Data introdusa este invalida.')


if __name__ == '__main__':
    unittest.main()

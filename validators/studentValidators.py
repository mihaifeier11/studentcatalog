class StudentError(Exception):
    pass

class StudentValidators:
    '''
        validateNrMatricol(nrMatricol, stList) - valideaza numarul matricol, acesta trebuie sa fie din 8 caractere, primele
        4 litere, urmatoarele 4 cifre si acesta trebuie sa existe deja
            nrMatricol - numarul matricol
            stList - lista de studenti

        validateNewNrMatricol(nrMatricol, stList) - valideaza numarul matricol, acesta trebuie sa fie din 8 caractere, primele
        4 litere, urmatoarele 4 cifre si acesta nu trebuie sa existe deja
            nrMatricol - numarul matricol
            stList - lista de studenti

        validateNume(nume) - valideaza numele, acesta trebuie sa contina doar caractere si spatii
            nume - numele studentului

        validateGrupa(grupa) - valideaza grupa, aceasta trebuie sa contina doar cifre
            grupa - grupa studentului
    '''
    def __init__(self):
        pass

    def validateNrMatricol(self, nrMatricol, stList):
        exist = False
        for elem in stList:
            if elem == nrMatricol:
                exist = True
        if exist == False:
            raise StudentError('Studentul cu acest numar matricol nu exista.')

        if len(nrMatricol) != 8 or nrMatricol[:4].isalpha() == False or nrMatricol[4:].isdigit() == False:
            raise StudentError('Numarul matricol introdus este invalid.')

    def validateNewNrMatricol(self, nrMatricol, stList):
        for elem in stList:
            if elem == nrMatricol:
                raise StudentError('Studentul cu acest numar matricol exista deja.')

        if len(nrMatricol) != 8 or nrMatricol[:4].isalpha() == False or nrMatricol[4:].isdigit() == False:
            raise StudentError('Numarul matricol introdus este invalid.')


    def validateNume(self, nume):
        name = nume.split(' ')
        if name[0].isalpha() == False or name[1].isalpha() == False:
            raise StudentError('Numele introdus nu este invalid. Acesta poate contine doar litere si spatii.')


    def validateGrupa(self, grupa):
        if grupa.isdigit() == False:
            raise StudentError('Grupa introdusa nu este valida. Aceasta poate contine doar cifre.')
